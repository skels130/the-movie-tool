﻿
'Import resources needed. Remind me to add a link to IMDB.dll
Imports vbIMDB.IMDb_Scraper
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Threading


Public Class Form1
    'Code for multithreading
#Region "members"
    Private getMovie_Thread As Threading.Thread
    Private Delegate Sub getMovie_Add_Delegate(ByVal itm As ListViewItem, ByVal i As Integer)
#End Region
    'Declare variable. List of Files and associated data. See end of code for moviePathclass
    Dim workingList As New List(Of moviePath)
    'Form load, disable main button
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Dim movie As New IMDb("The Usual Suspects", False)
        ' lstTest.Items.Add(movie.Id & " " & movie.Year)
        btnRun.Enabled = False

    End Sub
    'Sub that parses file names and removes words from textfile resource. Eventually will implement GUI changes of textfile 
    Sub parseFileName()
        Dim words As New List(Of String)
        'Call sub to read text file and return array of words
        loadDeleteWords(words)
        'loop through each file
        For i = 0 To (workingList.Count - 1)
            'call sub to parse and delete symbols
            parseSymbols(workingList(i).currentName)
            'variable to temporarily be able to manipulate. Could probably assign .currentName to .correctedName, but this seems safer
            Dim tempWord As String = workingList(i).currentName
            'Loop through every word loaded and remove from file name
            For Each deleteWord As String In words

                tempWord = Replace(tempWord, deleteWord, "", 1, -1, Constants.vbTextCompare)

            Next
            'set tempword into correctedname variable
            workingList(i).correctedName = tempWord
        Next
    End Sub


    'Main sub that populates listview, and is second thread. 
    Sub getMovie()

        'Populate list with each current file name
        For i = 0 To workingList.Count - 1
            Dim itm As ListViewItem
            Dim str(1) As String

            str(0) = (workingList(i).currentNameExt)
            str(1) = ("")
            itm = New ListViewItem(str)
            'Calls delegate to allow changing of listview
            Me.Invoke(New getMovie_Add_Delegate(AddressOf ListView_Add), itm, i)
        Next

        'Declare variable for a sleep timer
        Dim sleepCounter As Integer = 0
        'Loop through each file now, and call sub that parses IMDB. 
        For i = 0 To workingList.Count - 1
            'call sub that decides if it should search IMDb, and then returns results
            getImdb(i, sleepCounter)
            'Fill up the Listview
            Dim itm As ListViewItem
            Dim str(1) As String
            str(0) = (workingList(i).currentNameExt)
            str(1) = (workingList(i).IMDbName & " " & workingList(i).correctYear & workingList(i).currentExt)
            itm = New ListViewItem(str)
            'lstTest.Items.Add(itm)
            'Call delegate that changes previously assigned row
            Me.Invoke(New getMovie_Add_Delegate(AddressOf ListView_Change), itm, i)
        Next
    End Sub


    'Sub that checks if it should search IMDb, and returns results
    Private Sub getImdb(ByVal i As Integer, ByRef sleepCounter As Integer)
        'check ext to see if it should skip. Need to play with this, and might work better as a function (not a sub) and a loop/array. 
        Select Case True
            Case workingList(i).currentExt = ".txt"
                workingList(i).IMDbName = "Delete File"
                workingList(i).deleteFile = True
            Case workingList(i).currentExt = ".nfo"
                workingList(i).IMDbName = "Delete File"
                workingList(i).deleteFile = True
            'this case uses number wildcards to skip over multipart rar files
            Case workingList(i).currentExt Like ".r##"
                workingList(i).IMDbName = "Skip File"
                workingList(i).skipFile = True
            Case workingList(i).currentExt = ".rar"
                workingList(i).IMDbName = "Skip File"
                workingList(i).skipFile = True
            Case workingList(i).currentExt = ".jpg"
                workingList(i).IMDbName = "Delete File"
                workingList(i).deleteFile = True
            Case workingList(i).currentExt = ".png"
                workingList(i).IMDbName = "Delete File"
                workingList(i).deleteFile = True
            Case workingList(i).currentExt = ".sfv"
                workingList(i).IMDbName = "Delete File"
                workingList(i).deleteFile = True
            Case Else
                'Probably a movie file by this point. update status bar and begin searching via imdb library.
                lblText.Text = "Searching for " & workingList(i).correctedName
                Dim info As New IMDb(workingList(i).correctedName, False)
                workingList(i).correctYear = "(" & info.Year & ")"
                workingList(i).IMDbName = info.Title
                sleepCounter += 1
                'Not sure if necessary due to IP spoofing, but added a 8 sec sleep every 4 searches. need to investigate. 
                If sleepCounter = 4 Then
                    Threading.Thread.Sleep(8000)
                    sleepCounter = 0
                End If

                lblText.Text = "Done!"
        End Select
    End Sub

    'sub to delegate adding to list view
    Private Sub ListView_Add(ByVal itm As ListViewItem, ByVal i As Integer)
        lstTest.Items.Add(itm)
        ' lstTest.Items(i).Checked = True
    End Sub

    'sub to delegate changing list view. Also checks the box by default.
    Private Sub ListView_Change(ByVal itm As ListViewItem, ByVal i As Integer)
        lstTest.Items(i) = (itm)
        lstTest.Items(i).Checked = True
    End Sub

    'MAIN SUB THAT IS TRIGGERED BY DRAG DROP. Here's the meat of the program
    Sub dragDropMain(ByVal sender As Object, ByVal e As _
    System.Windows.Forms.DragEventArgs)
        'This bit recursively scans what is dropped into the program. Somewhere in here is where I will need to figureout how to deal with 
        'subdirectory renaming
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            'clear previous workload
            workingList.Clear()
            lstTest.Items.Clear()
            Dim myFiles As String() = e.Data.GetData(DataFormats.FileDrop)
            For Each attributes As String In myFiles

                If System.IO.Directory.Exists(attributes) Then
                    ' MsgBox(attributes)
                    'Its a directory

                    'Call two functions that scan each folder, And also one that scans the folder directly. the scanFolder sub
                    'doesn't scan the directory actually dropped for files. 
                    scanParent(attributes)
                    scanFolders(attributes)
                ElseIf System.IO.File.Exists(attributes) Then
                    workingList.Add(New moviePath(attributes))
                    ' MsgBox("it's a file")
                    'Its a file
                End If
            Next
            'Calls to parse filename for symbols and words we wish to remove
            parseFileName()


            'Here's where we start multithreading. calls the sub to start searching IMDB and filling our listview
            If getMovie_Thread Is Nothing Then
                getMovie_Thread = New Threading.Thread(AddressOf getMovie)
                getMovie_Thread.IsBackground = True
                getMovie_Thread.Start()
            End If


            'Enable button that will change names. Need to learn how to use multithreading tags to enable only after getMovie sub is done,
            'or create another delegate. (maybe)
            btnRun.Enabled = True


        End If
    End Sub
    'Sub that removes symbols. Pretty self explanitory 
    Sub parseSymbols(ByRef movie As String)
        Dim symbolsDel As String() = {"[", "]", "-", "."}
        For Each deleteSymbol As String In symbolsDel
            movie = (Replace(movie, deleteSymbol, " "))
            'lstTest.Items.Add(movie)
        Next


    End Sub

    'Sub that loads words from a text file.
    Sub loadDeleteWords(ByRef words As List(Of String))
        'load textfile. Need to figure out how to use non absolute file location (for installer/other people) 
        'for now, put .txt file in location and change string. 
        Dim lines As String = File.ReadAllText("C:\Users\Stefan\Documents\Visual Studio 2013\Projects\The Movie Tool\The Movie Tool\keywordsHardcodedPar.txt")
        ' Dim words As New List(Of String)

        'use regex logic to parse strings between sets of " " marks
        Dim regMatch As MatchCollection = Regex.Matches(lines, """(.*?)\""")


        ' Based on code found here: http://stackoverflow.com/questions/28156918/words-from-text-file-as-variables-in-a-checkedlistbox
        Dim listOfSecondWords = New List(Of String)()
        'Load those babies into a list
        For Each w As Match In regMatch
            Dim symbolDel As String = """"
            Dim tempWord As String = ""
            tempWord = (Replace(w.Value, symbolDel, ""))
            words.Add(tempWord)
        Next
    End Sub

    'required to make mouse correctly change pointers. need to look into possible issues with not releasing. Probs fixed with multithreading
    'already.
    Private Sub lstTest_DragEnter(ByVal sender As Object, ByVal e As _
System.Windows.Forms.DragEventArgs) Handles lstTest.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    'Sub that is called when files are dropped.
    Private Sub lstTest_DragDrop(ByVal sender As Object, ByVal e As _
    System.Windows.Forms.DragEventArgs) Handles lstTest.DragDrop
        'nothing fun here. just calls another sub. seemed smart for organizational purps. May be able to move calls to other subs here for 
        'nice organization.
        dragDropMain(sender, e)



    End Sub






    'Sub to scan folder dropped into program
    'Superduper thanks to Bradley_Uffner http://stackoverflow.com/questions/31481879/working-with-multiple-properties-of-array-vb-net
    Sub scanParent(ByVal sDir As String)
        Dim f As String

        Try

            For Each f In Directory.EnumerateFiles(sDir)
                workingList.Add(New moviePath(f))
            Next
        Catch excpt As System.Exception
            MessageBox.Show("It didn't work jackass" & excpt.ToString)

        End Try

    End Sub

    'sub to scan in each folder in the parent folder.
    Sub scanFolders(ByVal sDir As String)
        Dim d As String
        Dim f As String

        Try
            For Each d In Directory.EnumerateDirectories(sDir)
                For Each f In Directory.EnumerateFiles(d)
                    workingList.Add(New moviePath(f))
                Next
                scanFolders(d)
            Next
        Catch excpt As System.Exception
            MessageBox.Show("It didn't work jackass")

        End Try
    End Sub

    'this function is what acutally accesses the IMDB library.
    Function getMovie(movieName As String)
        Dim movie As New IMDb(movieName, False)

        Return (movie)
    End Function





    'Sub for button press to process files. 
    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        'Sets each item's change name boolean based on checkboxes. Probs could be better programed, but I did this part at like 3:30 AM,
        'so cut me some fucking slack. 
        For Each item As ListViewItem In lstTest.Items
            Dim indx As Integer = item.Index
            If item.Checked = True Then
                workingList(indx).changeName = True
            Else
                workingList(indx).changeName = False
            End If
        Next

        'Stub from testing. 
        ' For i = 0 To workingList.Count - 1
        'ListBox1.Items.Add(workingList(i).changeName)

        'Next
        'Here's the big loop. Change names and folders. Might be better to rewrite with folder creation, but we'll see.
        For i = 0 To workingList.Count - 1
            'to meet both conditions, would be a movie or file to delete that was checked
            If workingList(i).changeName = True And workingList(i).skipFile = False Then
                'Temp var for new name
                Dim newName As String = (workingList(i).IMDbName & " " & workingList(i).correctYear & workingList(i).currentExt)

                'stub from testing
                'ListBox1.Items.Add(workingList(i).currentPath & ", " & newName)
                'ListBox1.Items.Add(newName)

                'This bit deletes files that are supposed to get deleted. 
                If workingList(i).deleteFile = True Then
                    My.Computer.FileSystem.DeleteFile(workingList(i).currentPath)

                End If

                'Here is where we rename files if they still exist. reordering this bit above the deletefile section would most likely
                'remove need for try catch block.
                Try
                    'Commented out for testing, uncomment next line to rename files
                    '   My.Computer.FileSystem.RenameFile(workingList(i).currentPath, newName)
                Catch
                End Try

            End If
        Next
        'This loop is seperate to avoid issues with changing directories before files.
        For i = 0 To workingList.Count - 1
            'Not sure why I programmed this to check deleteFile = false, but it probably fixed something. 

            If workingList(i).changeName = True And workingList(i).deleteFile = False Then

                Dim newDir As String = (workingList(i).IMDbName & " " & workingList(i).correctYear)
                'ListBox1.Items.Add(workingList(i).currentPath & ", " & newName)
                'ListBox1.Items.Add(newName)

                'ListBox1.Items.Add(workingList(i).directory)
                'My.Computer.FileSystem.RenameFile(workingList(i).currentPath, newName)
                'cant remember if try block is needed, but I think it threw an error because of multiple files in one directory
                Try
                    '  My.Computer.FileSystem.RenameDirectory(workingList(i).directory, newDir)
                Catch
                End Try

            End If
        Next
    End Sub


End Class

'Heres a class for file information. 
'Thanks to Plutonix and Idle_Mind http://stackoverflow.com/questions/31481879/working-with-multiple-properties-of-array-vb-net/31484833#31484833
Public Class moviePath
    'sub to add in new file information.
    Public Sub New(ByVal FullFilePath As String)
        Me.currentPath = FullFilePath
        Me.currentName = Path.GetFileNameWithoutExtension(FullFilePath)
        Me.currentNameExt = Path.GetFileName(FullFilePath)
        Me.currentExt = Path.GetExtension(FullFilePath)
        Me.directory = Path.GetDirectoryName(FullFilePath)
    End Sub

    Public currentPath As String
    Public currentNameExt As String
    Public currentName As String
    Public currentExt As String
    Public correctedName As String
    Public correctYear As String
    Public IMDbName As String
    Public changeName As Boolean = False
    Public directory As String
    Public deleteFile As Boolean = False
    Public skipFile As Boolean = False
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.lstTest = New System.Windows.Forms.ListView()
        Me.originalName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.newName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.stsStrip = New System.Windows.Forms.StatusStrip()
        Me.lblText = New System.Windows.Forms.ToolStripStatusLabel()
        Me.stsStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(693, 426)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 23)
        Me.btnRun.TabIndex = 2
        Me.btnRun.Text = "Rename"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'lstTest
        '
        Me.lstTest.AllowDrop = True
        Me.lstTest.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.originalName, Me.newName})
        Me.lstTest.Location = New System.Drawing.Point(1, 3)
        Me.lstTest.MultiSelect = False
        Me.lstTest.Name = "lstTest"
        Me.lstTest.Size = New System.Drawing.Size(781, 446)
        Me.lstTest.TabIndex = 3
        Me.lstTest.UseCompatibleStateImageBehavior = False
        Me.lstTest.View = System.Windows.Forms.View.Details
        '
        'originalName
        '
        Me.originalName.Text = "Original File"
        Me.originalName.Width = 387
        '
        'newName
        '
        Me.newName.Text = "New File"
        Me.newName.Width = 387
        '
        'stsStrip
        '
        Me.stsStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblText})
        Me.stsStrip.Location = New System.Drawing.Point(0, 452)
        Me.stsStrip.Name = "stsStrip"
        Me.stsStrip.Size = New System.Drawing.Size(780, 22)
        Me.stsStrip.TabIndex = 4
        Me.stsStrip.Text = "stsStrip"
        '
        'lblText
        '
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(42, 17)
        Me.lblText.Text = "Status:"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(780, 474)
        Me.Controls.Add(Me.stsStrip)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.lstTest)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.stsStrip.ResumeLayout(False)
        Me.stsStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents lstTest As System.Windows.Forms.ListView
    Friend WithEvents originalName As System.Windows.Forms.ColumnHeader
    Friend WithEvents newName As System.Windows.Forms.ColumnHeader
    Friend WithEvents stsStrip As StatusStrip
    Friend WithEvents lblText As ToolStripStatusLabel
End Class
